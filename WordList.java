import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WordList {

  /*
  This class implements a word list that can be searched to see if a specific
  word is in it. In this program it'll be used to store a very large list, the 
  entire English language; however, there's nothing about this implementation 
  that wouldn't work perfectly fine for any word list. I didn't bother to add 
  methods for building up the list, though, only for loading it from a text 
  file (one word per line), though builder methods would be easily added.
  */
  
  protected List<String> AllTheWords;

  //////////////////////////////////////////////////////////////////////////
  // Default constructor creates an empty list.
  public WordList() {
    AllTheWords = new ArrayList<>();
  }

  //////////////////////////////////////////////////////////////////////////
  // This constructor gets a filename and loads the file into the list as it
  // creates it.
  public WordList(String filename) {
    this();
    try
    {
      for (String line : Files.readAllLines(Paths.get(filename))) {
          AllTheWords.add(line.toLowerCase());
      }      
    }
    catch (Exception e)
    {
      System.out.println(e.toString());
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // Method to return the length of the list, useful for testing.
  public int Length() {
    return AllTheWords.size();
  }

  //////////////////////////////////////////////////////////////////////////
  // Method to determine if a word is in the list (doesn't matter where).
  public boolean WordInList(String target) {
    String targetLower = target.toLowerCase();
    return AllTheWords.contains(targetLower);
  }

  //////////////////////////////////////////////////////////////////////////
  // Simple substring match to determine if a word is a prefix of another word
  // (note that for these purposes, we assume a word qualifies as its own prefix)
  private boolean StringStartsWith(String candidate, String prefix) {
    if (candidate.length() < prefix.length()) return false;
    String trimmedCandidate = candidate.substring(0,prefix.length());
    if (trimmedCandidate.equals(prefix)) return true;
    return false;
  }

  //////////////////////////////////////////////////////////////////////////
  // Method to determine if a prefix matches any words in the list
  // (used for an optimization: no sense in checking all the words
  // that start XYZ so we can stop a search early).
  public boolean PrefixInList(String target) {
    String targetLower = target.toLowerCase();
    int i;
    for (i = 0; i < AllTheWords.size(); i++)
    {
      if (StringStartsWith(AllTheWords.get(i), targetLower)) return true;
    }
    return false;
  }

  //////////////////////////////////////////////////////////////////////////
  // main function, used only for unit testing
  public static void main(String[] args) {
    WordList a = new WordList();
    System.out.println("a = " + a.Length());
    WordList b = new WordList("wordlist.txt");
    System.out.println("b = " + b.Length());
    if (b.WordInList("paper")) System.out.println("paper is a word");
    if (!b.WordInList("peper")) System.out.println("peper is not a word");
    if (b.StringStartsWith("pepper","pepp")) 
        System.out.println("pepper starts with pepp");
    if (!b.StringStartsWith("pepper","pypp")) 
        System.out.println("pepper doesn't start with pypp");
    if (b.PrefixInList("pepp")) System.out.println("Something starts with pepp");
    if (!b.PrefixInList("pypp")) System.out.println("Nothing starts with pypp");
  }

}