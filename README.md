# GridSolver #

This is a Java application that uses only the console for output and the command line for input, since the purpose in writing it was just to explore Java classes and refresh myself on its syntax, not to build a Windows, web, or mobile app.

### The application ###

Given a "Boggle"-style grid of letters like this:

    D G H I  
    K L P S  
    Y E U T  
    E O R N  

Exhaustively, but efficiently, find all English words which appear in the grid as a series of letters adjacent to one another, including diagonally. You may not re-use any tile. In the diagram above, find the word YORE (starts in the third row on the left, then go southeast, east, northwest), and SPURT (starts on the second row at the end), but note that SPURTS isn't valid since it would reuse the same S it started with.

For this solution I assume it's okay to find the same word more than once if it can be created in more than one unique path. For instance, LYE can be formed two ways, both starting from the L on the second row, then southwest, then either south or east. (You could always pipe the output into uniq to remove the duplicates if this isn't desired.)
