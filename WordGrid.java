import java.util.Random;

public class WordGrid extends WordList {

  /*
  This class implements a four-by-four Boggle-style letter grid.
  It has constructors to create grids (either a standard grid, a
  specified grid, or a random one), print the grid, extract a word
  from a series of positions, and most importantly, to create a
  list of all words found in the grid (using adjacent letters without
  repeating a tile) that are also in another word list.
  */

  public static final int GRID_SIZE = 4;
  
  protected char[] TheGrid;

  //////////////////////////////////////////////////////////////////////////
  // Constructor that loads a specified sixteen characters into the grid.
  public WordGrid(String gridLetters) {
    if (gridLetters.length() != GRID_SIZE*GRID_SIZE)
    {
      System.out.println("Incorrect number of letters provided to initialize the grid.");
      return;
    }
    TheGrid = new char[GRID_SIZE*GRID_SIZE];
    for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
    {
      TheGrid[i] = Character.toLowerCase(gridLetters.charAt(i));
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // Default constructor loads a predetermined grid.
  public WordGrid() {
    this("DGHIKLPSYEUTEORN"); 
  }

  //////////////////////////////////////////////////////////////////////////
  // Constructor that loads a random grid.
  // Note that the parameter itself is ignored, and is just used to select 
  // this constructor (a bit hackish).
  public WordGrid(int randomize) {
    TheGrid = new char[GRID_SIZE*GRID_SIZE];
    Random rand = new Random();
    for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
    {
      TheGrid[i] = (char) (rand.nextInt(26) + 97);
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // Method to print out the grid.
  public void PrintTheGrid() {
    for (int row = 0; row < GRID_SIZE; row++)
    {
      System.out.print("  ");
      for (int col = 0; col < GRID_SIZE; col++)
        System.out.print("+---");
      System.out.println("+");
      System.out.print("  ");
      for (int col = 0; col < GRID_SIZE; col++)
      {
        System.out.print("| " + TheGrid[row * GRID_SIZE + col] + " ");
      }
      System.out.println("|");
    }
    System.out.print("  ");
    for (int col = 0; col < GRID_SIZE; col++)
      System.out.print("+---");
    System.out.println("+");
  }

  //////////////////////////////////////////////////////////////////////////
  // Method that returns the string from a list of positions.
  public String WordFromPositions(int[] Positions) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < Positions.length; i++)
    {
      result.append(TheGrid[Positions[i]]);
    }
    return result.toString();
  }

  //////////////////////////////////////////////////////////////////////////
  // Method that returns the new position from a starting position and
  // direction, or -1 if this would take us outside the grid; this is
  // used internally in the grid solver.
  private int AdjacentPosition(int OldPosition, int Direction) {
    // convert a position to row and column
    int row = OldPosition / GRID_SIZE;
    int col = OldPosition % GRID_SIZE;

    // an array is faster than doing the math, and this is
    // inside a frequently-executed function so a good place
    // to throw away a few dozen bytes to gain that speed
    int[] DirectionRowDelta = {-1, -1, -1,  0, 0,  1, 1, 1};
    int[] DirectionColDelta = { -1, 0, 1, -1, 1, -1, 0, 1 };

    // shift
    row += DirectionRowDelta[Direction];
    col += DirectionColDelta[Direction];

    // throw a -1 if we're now outside the grid
    if (row < 0 || row >= GRID_SIZE || col < 0 || col >= GRID_SIZE) return -1;

    // convert back to position
    return row * GRID_SIZE + col;
  }

  //////////////////////////////////////////////////////////////////////////
  // A private method to check an integer array for a value already present,
  // used here to ensure that we aren't reusing a tile
  // (Java probably provides a native method to do this)
  private boolean ValueInArray(int value, int[] IntArray) {
    for (int i = 0; i < IntArray.length; i++ )
    {
      if (IntArray[i] == value) return true;
    }
    return false;
  }

  //////////////////////////////////////////////////////////////////////////
  // Given a series of positions used so far and a direction, returns the 
  // position in that direction, but only if it's a valid position not 
  // already present in the list; this is used while recursing to avoid 
  // going around in loops and thus having endless recursion.
  private int AdjacentUnusedPosition(int[] Positions, int Direction) {
    int NewPosition = AdjacentPosition(Positions[Positions.length-1], Direction);
    if (NewPosition == -1) return -1;
    if (ValueInArray(NewPosition, Positions)) return -1;
    return NewPosition;
  }

  //////////////////////////////////////////////////////////////////////////
  // For a given list of positions already explored, report it if it's a word,
  // then see if adding one more position will make a word in each possible
  // direction (excluding crossing back over places you've already been,
  // or falling off the grid). This is a function that will recurse extensively,
  // and is where the main work of the program is done.
  private void RecursePosition(int[] Positions, WordList AllTheWords) {

    // First, if this is a word, report it to the output.
    String AllegedWord = WordFromPositions(Positions);
    if (AllTheWords.WordInList(AllegedWord)) System.out.println(AllegedWord);
    // Doing this first ensures single-letter words are found (e.g., I)

    // If the grid allows the same word to be created by differing
    // paths, it will be reported each time. For instance, in
    // the supplied grid, LYE can be made two ways (5 8 9 and 5 8 12)
    // and so will be listed twice. I consider this a feature, but
    // if it's considered a bug, you could always pipe the output
    // into uniq! (Or solve it with an internal cache, but that's a
    // much less modular solution, so I haven't done that here.)

    // Next, if the list is already sixteen elements, take an early exit
    // Note: if there are no valid directions, we won't recurse anyway,
    // so this is just an optimization, and the code would work without
    // it (ideally we'd prove this with red-green refactoring).
    if (Positions.length == GRID_SIZE*GRID_SIZE) return;

    // Another optimization is to abandon a search entirely if nothing starts 
    // with the letters we already have.
    if (!AllTheWords.PrefixInList(AllegedWord)) return;

    // For each possible direction, try adding that direction
    for (int Direction = 0; Direction <= 7; Direction++) {
      int NewPosition = AdjacentUnusedPosition(Positions, Direction);
      if (NewPosition != -1) {
        int NewLength = Positions.length + 1;
        int[] NewPositions = new int[NewLength];
        for (int i = 0; i < Positions.length; i++) NewPositions[i] = Positions[i];
        NewPositions[NewLength-1] = NewPosition;
        RecursePosition(NewPositions, AllTheWords);
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // This method is the heart of the program: given a grid it will print all
  // the words in it (it relies on the WordList class for the master word
  // list). It relies on RecursePosition to do the real work; it just
  // presents it with 16 starting position lists, each representing a
  // path of a single tile.
  public void FindWordsInGrid() {
    // declare and construct a WordList
    WordList AllTheWords = new WordList("wordlist.txt");

    // try solutions starting at each grid location
    for (int StartPosition = 0; StartPosition < GRID_SIZE*GRID_SIZE; StartPosition++)
    {
      int[] Positions = new int[1];
      Positions[0] = StartPosition;
      RecursePosition(Positions, AllTheWords);
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // Some code I wrote to unit test bits of this program as I was writing
  // it. Often you use main() for that, when writing a class that will be
  // part of something else, but in this case, main() is the real deal.
  private void UnitTesting() {
    System.out.println("An ugly grid:");
    WordGrid b = new WordGrid("ABCDEFGHIJKLMNOP");
    b.PrintTheGrid();
    System.out.println("A random grid:");
    WordGrid c = new WordGrid(1);
    c.PrintTheGrid();
    int[] posns = {5,2,3,1,6};
    System.out.println(c.WordFromPositions(posns));
    System.out.println("Starting from position 5: ");
    for (int d = 0; d <= 7; d++) {
      System.out.print(c.AdjacentPosition(5, d) + " ");
    }
    System.out.println("\nShould be 0 1 2 4 6 8 9 10");
    System.out.println("Starting from position 4: ");
    for (int d = 0; d <= 7; d++)
    {
      System.out.print(c.AdjacentPosition(4, d) + " ");
    }
    System.out.println("\nShould be -1 0 1 -1 5 -1 8 9");
    int[] Positions = { 3, 6, 7 };
    System.out.println("Starting from positions 3 6 7: ");
    for (int d = 0; d <= 7; d++)
    {
      System.out.print(c.AdjacentUnusedPosition(Positions, d) + " ");
    }
    System.out.println("\nShould be 2 -1 -1 -1 -1 10 11 -1");
  }

  //////////////////////////////////////////////////////////////////////////
  // Creates a grid based on the arguments passed, then solves it.  
  public static void main(String[] args) {
    WordGrid MyGrid;

    if (args.length == 0)
    {
      MyGrid = new WordGrid();
    }
    else if (args[0].equals("random"))
    {
      MyGrid = new WordGrid(1);
    }
    else if (args[0].length() == GRID_SIZE*GRID_SIZE)
    {
      MyGrid = new WordGrid(args[0]);
    }
    else 
    {
      System.out.println("Invalid grid specification ignored.");
      MyGrid = new WordGrid();
    }

    System.out.println("\nFind all words present in this grid:");
    MyGrid.PrintTheGrid();
    
    System.out.println("\nSolutions:");
    MyGrid.FindWordsInGrid();
  }

}